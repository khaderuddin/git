The ViewModel, which decribes the basic use and the functinality for the application.

using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace CityPopulation
{
    public class ViewModel : INotifyPropertyChanged
    {
        DataBase dataBase = DataBase.GetInstance();

        private BindingList<CityData> cityList = new BindingList<CityData>();
        public BindingList<CityData> CityList
        {
            get { return cityList; }
            set { cityList = value; NotifyChanged(); }
        }

        private CityData selectedCity;
        public CityData SelectedCity
        {
            get { return selectedCity; }
            set { selectedCity = value; NotifyChanged(); }
        }

        private CityData onEditCity ;
        public CityData OnEditCity
        {
            get { return onEditCity; }
            set { onEditCity = value; NotifyChanged(); }
        }

        private string columnOneName="City Name";
        public string ColumnOneName
        {
            get { return columnOneName; }
            set { columnOneName = value; NotifyChanged(); }
        }

        private string columnTwoName = "City Population";
        public string ColumnTwoName
        {
            get { return columnTwoName; }
            set { columnTwoName = value; NotifyChanged(); }
        }

        private string highestCity="No City";
        public string HighestCity
        {
            get { return highestCity; }
            set { highestCity = value; NotifyChanged(); }
        }

        private int highestPopulation=0;
        public int HighestPopulation
        {
            get { return highestPopulation; }
            set { highestPopulation = value; NotifyChanged(); }
        }

        private int totalPopulation=0;
        public int TotalPopulation
        {
            get { return totalPopulation; }
            set { totalPopulation = value; NotifyChanged(); }
        }



        public void LoadFromFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Csv files (*.csv)|*.csv|All files (*.*)|*.*";
            openFileDialog.FileName = "city.csv";
            if (openFileDialog.ShowDialog() == true)
            {
                var cityData = File.ReadAllLines(openFileDialog.FileName);
                string[] dataDetail=cityData[0].Split(',');
                int tempPopulation = 0;
                TotalPopulation = 0;
                HighestPopulation = 0;
                ColumnOneName = dataDetail[0].Substring(1, dataDetail[0].Length - 2);
                ColumnTwoName = dataDetail[1].Substring(1, dataDetail[1].Length - 2);
                CityList.Clear();
                for (int i= 1;i < cityData.Length;i++)
                {
                    dataDetail = cityData[i].Split(',');
                    dataDetail[0] = dataDetail[0].Substring(1, dataDetail[0].Length - 2);
                    dataDetail[1] = dataDetail[1].Substring(1, dataDetail[1].Length - 2);
                    try
                    {
                        tempPopulation = Convert.ToInt32(dataDetail[1]);
                    }
                    catch
                    {
                        tempPopulation = 0;
                    }
                    var cityObject = new CityData(dataDetail[0], tempPopulation, Convert.ToInt32(dataDetail[2].Substring(1, dataDetail[2].Length - 2)));
                    CityList.Add(cityObject);
                    if (tempPopulation > HighestPopulation)
                    {
                        HighestPopulation = tempPopulation;
                        HighestCity = dataDetail[0];
                    }
                    TotalPopulation += tempPopulation;
                }
            }
        }

        public void GetHighestCity()
        {
            var maxCity = dataBase.FindHigest();
            HighestCity = maxCity.CityName;
            HighestPopulation = maxCity.Population;
        }

        public void GetTotal()
        {
            TotalPopulation = (int)dataBase.FindTotal();
        }

        public void InsertListToDatabase()
        {
            foreach(CityData cityInList in CityList)
            {
                dataBase.Add(cityInList);
            }
        }

        public void LoadFromDataBase()
        {
            CityList.Clear();
            dataBase.PopulateList(ref cityList);
            GetHighestCity();
            GetTotal();
        }

        public void AddOrEdit()
        {
            if (MessageBox.Show("Are you sure?",
                "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                if (OnEditCity.IsChange)
                {
                    dataBase.Update(SelectedCity.CityName, OnEditCity);
                    int indexOfCity = CityList.IndexOf(SelectedCity);
                    CityList.Remove(SelectedCity);
                    CityList.Insert(indexOfCity, OnEditCity);
                }
                else
                {
                    CityList.Add(OnEditCity);
                    dataBase.Add(OnEditCity);
                    if (HighestPopulation < OnEditCity.Population)
                    {
                        HighestPopulation = OnEditCity.Population;
                        HighestCity = OnEditCity.CityName;
                    }
                    TotalPopulation += OnEditCity.Population;
                }
                GetHighestCity();
                GetTotal();
            }  
        }

        public void Remove()
        {
            if (MessageBox.Show("Do you really want to delete this?",
                "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                dataBase.Delete(selectedCity.CityName);
                CityList.Remove(SelectedCity);
                GetHighestCity();
                GetTotal();
            }
        }
        #region Property Changed
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyChanged([CallerMemberName] string property = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion
    }
}